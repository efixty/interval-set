import java.util.*;

public class IntervalSet {

    private final List<IntervalNode> intervals;

    public IntervalSet() {
        this.intervals = new ArrayList<>();
    }

    public static class Interval {
        int low;
        int high;

        public Interval(int low, int high) {
            this.low = low;
            this.high = high;
        }

        public Interval copy() {
            return new Interval(this.low, this.high);
        }
    }

    private static class IntervalNode {
        Interval interval;
        int maxHighSubtree;

        public IntervalNode(int low, int high) {
            this.interval = new Interval(low, high);
            this.maxHighSubtree = high;
        }

        public boolean intersects(IntervalNode other) {
            return this.interval.low <= other.interval.high && this.interval.high >= other.interval.low;
        }
    }

    public Optional<Interval> add(int low, int high) {
        IntervalNode node = new IntervalNode(low, high);
        if (this.intervals.isEmpty()) {
            intervals.add(node);
            return Optional.empty();
        }
        int new_node_pos = this.intersects(0, this.intervals.size(), node);
        if (new_node_pos < 0) {
            this.intervals.add(-(new_node_pos + 1), node);
            correctSubtreesHigh(0, this.intervals.size());
            return Optional.empty();
        }
        return Optional.of(this.intervals.get(new_node_pos).interval.copy());
    }

    private int intersects(int start, int end, IntervalNode node) {
        if (end == start) {
            return -(start + 1);
        }
        int mid = mid(start, end);
        if (this.intervals.get(mid).intersects(node)) {
            return mid;
        } else if (start == mid) {
            final boolean isLeft = this.intervals.get(mid).interval.low > node.interval.low;
            return intersects(isLeft ? 0 : 1 + mid, (isLeft ? -1 : 0) + end, node);
        } else if (this.intervals.get(mid(start, mid)).maxHighSubtree < node.interval.low) {
            return intersects(mid + 1, end, node);
        } else {
            return intersects(start, mid, node);
        }
    }

    private int mid(int a, int b) {
        return (a + b) / 2 - (1 - (b - a) % 2);
    }

    private int correctSubtreesHigh(int start, int end) {
        if (end == start) {
            return Integer.MIN_VALUE;
        }
        int mid = mid(start, end);
        this.intervals.get(mid).maxHighSubtree = Math.max(
                this.intervals.get(mid).interval.high,
                Math.max(correctSubtreesHigh(start, mid), correctSubtreesHigh(mid + 1, end))
        );
        return this.intervals.get(mid).maxHighSubtree;
    }
}
